//
//  BaseTableViewController.swift
//  weibo
//
//  Created by 吕俊霖 on 15/11/9.
//  Copyright © 2015年 itcast. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController ,VisitorLoginViewDelegate{
    
    var userLogin = false
    var visitorLoginView :VisitorLoginView?
    
    
    override func loadView() {
        
        userLogin ? super.loadView() : loadVisitorView()
        
    }
    
    private func loadVisitorView(){
        

        
        visitorLoginView = VisitorLoginView()
        
        //设置代理
        
        visitorLoginView?.visitorDelegate = self
        
        view = visitorLoginView
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "注册", style: .Plain, target: self, action: "visitorWillRegister")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "登录", style: .Plain, target: self, action: "visitorWillLogin")
    }
    
    //实现协议方法t
    
    func visitorWillLogin() {
        print("123")
        let oauth = OAuthViewController()
        
        let nav = UINavigationController(rootViewController: oauth)
        
        presentViewController(nav, animated: true, completion: nil)
    }
    
    func visitorWillRegister() {
        print("321")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        

    }
}
