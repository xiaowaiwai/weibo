//
//  MessageTableViewController.swift
//  weibo
//
//  Created by 吕俊霖 on 15/11/8.
//  Copyright © 2015年 itcast. All rights reserved.
//

import UIKit

class MessageTableViewController: BaseTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        visitorLoginView?.setUIInfo("visitordiscover_image_message", title: "登录后，别人评论你的微博，发给你的消息，都会在这里收到通知")
    }

}
