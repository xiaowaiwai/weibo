//
//  MainViewController.swift
//  weibo
//
//  Created by 吕俊霖 on 15/11/8.
//  Copyright © 2015年 itcast. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let MaintabBar = MainTabBar()
        setValue(MaintabBar, forKey: "tabBar")
        
        
        addChildViewControllers()
        
    }

    private func addChildViewControllers() {
        

        addChildViewController(HomeTableViewController(), title: "首页" , imageName: "tabbar_home")
        addChildViewController(MessageTableViewController(), title: "消息" , imageName: "tabbar_message_center")
        addChildViewController(DiscoverTableViewController(), title: "发现" , imageName: "tabbar_discover")
        addChildViewController(ProfileTableViewController(), title: "我" , imageName: "tabbar_profile")
        
    }
    
    //利用函数重载
    private func addChildViewController(vc : UIViewController , title : String , imageName : String){
    
//    let home = HomeTableViewController()
    
    //实例化导航试图控制器
    
    let nav = UINavigationController(rootViewController: vc)
    
    vc.title = title
    
    vc.tabBarItem.image = UIImage(named: imageName)
    //更改选中图片
    vc.tabBarItem.selectedImage = UIImage(named: imageName + "_highlighted")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
    //选中字体颜色
    vc.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.orangeColor()], forState: UIControlState.Selected)
    
    addChildViewController(nav)
    }

}
