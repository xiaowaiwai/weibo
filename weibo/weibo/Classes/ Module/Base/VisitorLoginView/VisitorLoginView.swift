//
//  VisitorLoginView.swift
//  weibo
//
//  Created by 吕俊霖 on 15/11/9.
//  Copyright © 2015年 itcast. All rights reserved.
//


import UIKit


protocol VisitorLoginViewDelegate : NSObjectProtocol{
    
    //协议方法
    
//    登录方法
    func visitorWillLogin()
//    注册
    func visitorWillRegister()
    
}

class VisitorLoginView: UIView {
    
    //声明代理属性
    weak var visitorDelegate : VisitorLoginViewDelegate?
    
    @objc func loginDidbtnClick(){
       visitorDelegate?.visitorWillLogin()
    }
    
    @objc func registerDidbtnClick(){
        visitorDelegate?.visitorWillRegister()
    }
    
    //设置页面信息
    func setUIInfo(imageName : String? , title : String){
        
        iconView.hidden = false
        
        tipLabel.text = title
        
        if imageName != nil{
            circleView.image = UIImage(named: imageName!)
            bringSubviewToFront(iconView)
            iconView.hidden = true
        }else{
            starAnimation()
        }
    
        
    }
    
    private func starAnimation(){
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        anim.repeatCount = MAXFLOAT
        anim.toValue = 2 * M_PI
        anim.duration = 20
        //当动画结束，或者视图处于非活跃状态，动画不移除图层
        anim.removedOnCompletion = false
        
        circleView.layer.addAnimation(anim, forKey: nil)
        
    }

    init(){
        super.init(frame : CGRectZero)
        setupUI()
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    private func setupUI(){
        addSubview(circleView)
        addSubview(backView)
        addSubview(iconView)
        
        addSubview(tipLabel)
        addSubview(logingBtn)
        addSubview(registerBtn)
        
        
        //设置frame布局失效
        circleView.translatesAutoresizingMaskIntoConstraints = false
        iconView.translatesAutoresizingMaskIntoConstraints = false
        tipLabel.translatesAutoresizingMaskIntoConstraints = false
        logingBtn.translatesAutoresizingMaskIntoConstraints = false
        registerBtn.translatesAutoresizingMaskIntoConstraints = false
        backView.translatesAutoresizingMaskIntoConstraints = false
        
        //设置布局
        addConstraint(NSLayoutConstraint(item: circleView, attribute: NSLayoutAttribute.CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: circleView, attribute: NSLayoutAttribute.CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1.0, constant: -60))
        //iconView
        addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterX, relatedBy: .Equal, toItem: circleView, attribute: .CenterX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterY, relatedBy: .Equal, toItem: circleView, attribute: .CenterY, multiplier: 1.0, constant: 0))
        //label
        addConstraint(NSLayoutConstraint(item: tipLabel, attribute: NSLayoutAttribute.CenterX, relatedBy: .Equal, toItem: circleView, attribute: .CenterX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: tipLabel, attribute: NSLayoutAttribute.Top, relatedBy: .Equal, toItem: circleView, attribute: .Bottom, multiplier: 1.0, constant: 16))
        //设置label宽度
    addConstraint(NSLayoutConstraint(item: tipLabel, attribute: NSLayoutAttribute.Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 224))
        addConstraint(NSLayoutConstraint(item: tipLabel, attribute: NSLayoutAttribute.Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 40))
        
        //longingBtn设置
        addConstraint(NSLayoutConstraint(item: logingBtn, attribute: NSLayoutAttribute.Left, relatedBy: .Equal, toItem: tipLabel, attribute: .Left, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: logingBtn, attribute: NSLayoutAttribute.Top, relatedBy: .Equal, toItem: tipLabel, attribute: .Bottom, multiplier: 1.0, constant: 16))
        addConstraint(NSLayoutConstraint(item: logingBtn, attribute: NSLayoutAttribute.Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 100))
        addConstraint(NSLayoutConstraint(item: logingBtn, attribute: NSLayoutAttribute.Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 35))
         //registerBtn设置
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Right, relatedBy: .Equal, toItem: tipLabel, attribute: .Right, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Top, relatedBy: .Equal, toItem: tipLabel, attribute: .Bottom, multiplier: 1.0, constant: 16))
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 100))
        addConstraint(NSLayoutConstraint(item: registerBtn, attribute: NSLayoutAttribute.Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 35))
        
        //设置背景视图的约束

        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[backView]-0-|", options: [], metrics:nil , views: ["backView":backView]))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[backView]-(-45)-[registerBtn]", options: [], metrics: nil , views: ["backView":backView,"registerBtn":registerBtn]))
        
          //设置背景颜色
        backgroundColor = UIColor (white: 0.93, alpha: 1)
        
        //添加点击事件
        logingBtn.addTarget(self, action: "loginDidbtnClick", forControlEvents: .TouchUpInside)
        registerBtn.addTarget(self, action: "registerDidbtnClick", forControlEvents: .TouchUpInside)

    }
    
    private lazy var circleView : UIImageView = UIImageView (image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    private lazy var iconView : UIImageView = UIImageView (image: UIImage(named: "visitordiscover_feed_image_house"))
    private lazy var backView : UIImageView = UIImageView (image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
    
    private lazy var tipLabel :UILabel  = {
       let l = UILabel()
        l.text = "关注一些人，回这里看看有什么惊喜关注一些人，回这里看看有什么惊喜"
        l.textAlignment = NSTextAlignment.Center
        
        l.font = UIFont.systemFontOfSize(14)
        l.textColor = UIColor.lightGrayColor()
        l.numberOfLines = 0
        l.sizeToFit()
        return l
    
    }()
    private lazy var logingBtn :UIButton = {
        let btn = UIButton()
        btn.setTitle("登录", forState: .Normal)
        btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState:.Normal)
        btn.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        return btn
    }()
    private lazy var registerBtn :UIButton = {
        let btn = UIButton()
        btn.setTitle("注册", forState: .Normal)
        btn.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState:.Normal)
        btn.setTitleColor(UIColor.orangeColor(), forState: .Normal)
        return btn
    }()
}
