//
//  MainTabBar.swift
//  weibo
//
//  Created by 吕俊霖 on 15/11/9.
//  Copyright © 2015年 itcast. All rights reserved.
//

import UIKit



class MainTabBar: UITabBar {

    override init(frame: CGRect) {
      super.init(frame: frame)
    
    setupUI()
        
    }

    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        
    }
    
    
    private func setupUI(){
        addSubview(composeBtn)
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //手动修改
        let w = self.bounds.width / 5
        let h = self.bounds.height
        let rect = CGRect(x: 0, y: 0, width: w, height: h)
        
        var index :CGFloat = 0
        for subView in subviews{
            if subView.isKindOfClass(NSClassFromString("UITabBarButton")!){
                subView.frame = CGRectOffset(rect, index * w , 0)
//                index++
                
                index += index == 1 ? 2 : 1
            }
            
            
        }
        composeBtn.frame = CGRectOffset(rect, w * 2 , 0)
    }

    lazy var composeBtn : UIButton = {
        //自定义按钮
       let btn = UIButton()
        
        //设置按钮
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: UIControlState.Normal)
        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
        btn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: UIControlState.Normal)
        btn.setImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: UIControlState.Highlighted)
        btn.sizeToFit()
        
        return btn
    }()
}
